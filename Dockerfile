FROM node


RUN mkdir /skillbox
COPY package.json /skillbox
WORKDIR /skillbox
RUN yarn install
COPY . /skillbox

#Run yarn install

Run yarn test
Run yarn build
CMD yarn start

EXPOSE 3000
